#!/usr/bin/python

import argparse
import httplib2
import os
import tempfile
from sys import argv

from apiclient import discovery
from oauth2client import file
from oauth2client import client
from oauth2client import tools


class GCS_Downloader(object):
    API_VERSION = 'v1'

    # This file is built by downloading a JSON Credential file from:
    # console.developers.google.com -> <project> -> APIs & auth -> Credentials
    CLIENT_SECRETS_PATH = os.path.join(os.path.dirname(__file__), 'client_secrets.json')

    # Path to the credentials file that is generated on the first ever run.
    CREDENTIALS_PATH = os.path.join(os.path.expanduser('~'), '.credentials.json')

    # Set up a Flow object to be used for authentication.
    FLOW = client.flow_from_clientsecrets(
        CLIENT_SECRETS_PATH,
        scope=[
            #'https://www.googleapis.com/auth/devstorage.full_control',
            'https://www.googleapis.com/auth/devstorage.read_only',
            #'https://www.googleapis.com/auth/devstorage.read_write',
        ],
        message=tools.message_if_missing(CLIENT_SECRETS_PATH)
    )

    # Parser for command-line arguments.
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
        parents=[tools.argparser]
    )

    def __init__(self, bucket_name, file_name, out_file_path=None):
        """
        Initialize and do the things.

        :param bucket_name: The bucket (root directory) where the file is located within storage.
        :type bucket_name: str
        :param file_name: The filename to download.
        :type file_name: str
        :param out_file_path: The path that should end up with the contents of the dump.
        :type out_file_path: str
        """
        self.bucket_name = bucket_name
        self.file_name = file_name
        self.out_file_path = out_file_path or os.path.join(tempfile.gettempdir(), file_name)
        if out_file_path is None:
            print('No out_file_path provided, so downloading to {} ...'.format(self.out_file_path))
        self.credentials = self.authorize()
        self.download(self.create_http_service(), self.out_file_path)

    def authorize(self):
        """
        If the credentials don't exist or are invalid, run through the native client flow.
        The Storage object will ensure that successful credentials will be written back to a file.
        """
        storage = file.Storage(self.CREDENTIALS_PATH)
        credentials = storage.get()
        if credentials is None or credentials.invalid:
            flags = self.parser.parse_args([])
            credentials = tools.run_flow(self.FLOW, storage, flags)

        return credentials

    def create_http_service(self):
        """
        Create a httplib2.Http object to handle HTTP requests and attach it to an http service.
        """
        http = httplib2.Http()
        http = self.credentials.authorize(http)
        service = discovery.build('storage', self.API_VERSION, http=http)

        return service

    def download(self, http_service, out_file_path):
        """
        Dump the cloud storage file to file on the local disk.

        :param http_service: An authorized httplib2.Http instance.
        :type http_service: googleapiclient.discovery.Resource
        :param out_file_path: The path that should end up with the contents of the dump.
        :type out_file_path: str
        """
        req = http_service.objects().get_media(bucket=self.bucket_name, object=self.file_name)
        if not req:
            raise ValueError('Could not download bucket={} object={}'.format(self.bucket_name, self.file_name))

        with open(out_file_path, mode='wb') as fp:
            fp.write(req.execute())


if __name__ == '__main__':
    try:
        GCS_Downloader(argv[1], argv[2], argv[3])
    except IndexError:
        try:
            GCS_Downloader(argv[1], argv[2])
        except IndexError:
            print '{} <bucket-name> <file-name>'.format(argv[0])
